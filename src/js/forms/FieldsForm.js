// создаем объект полей формы с плейсхолдерами
export const fieldsForm = {
    fullName: {
       type: "text",
       placeholder: "ПІБ",
       isRequired: true,
    },
    purpose: {
       type: "text",
       placeholder: "Ціль візиту",
       isRequired: true
    },
    desc: {
       placeholder: "Короткий опис візиту",
       isRequired: true
    },
    pressure: {
       type: "text",
       placeholder: "Звичайний тиск",
       isRequired: true
    },
    weightIndex: {
       type: "text",
       placeholder: "Індекс маси тіла",
       isRequired: true
    },
    illness: {
       placeholder: "Перенесені захворювання сердцево-судинної системи",
       isRequired: true
    },
    age: {
       type: "text",
       placeholder: "Вік",
       isRequired: true
    },
    lastDateVisit: {
       type: "text",
       placeholder: "Дата останнього візиту",
       isRequired: true
    },
    submit: {
       type: "submit"
    },
    priority: [
       "Виберіть терміновість",
       "Звичайна",
       "Пріоритетна",
       "Невідкладна"
    ]
 }